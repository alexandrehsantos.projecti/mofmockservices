package br.com.cipbancos.scg.ws;

import java.util.Date;

import javax.xml.ws.Endpoint;

import br.com.cipbancos.scg.util.MessageUtil;

public class PublishService {

	private static final String HTTP_LOCALHOST_9080_WEBSERVICE_SCG_SCG_SERVICE = "http://localhost:9080/webservice-scg/SCGService";

	public static void main(String[] args) {

		MockCip mockCip = new MockCip();

		Endpoint.publish(HTTP_LOCALHOST_9080_WEBSERVICE_SCG_SCG_SERVICE, mockCip);

		MessageUtil.startMessages();
	}

	public static void resquestMessages(String fileContent) {
		System.out.println("\n Requisição recebida as..." + new Date());
		System.out.println("-----------Inicio do conteúdo -----------");
		System.out.println(fileContent);
		System.out.println("----------Término do conteúdo -----------");
	}

}
