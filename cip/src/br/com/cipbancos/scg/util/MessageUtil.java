package br.com.cipbancos.scg.util;

import java.util.Date;

public class MessageUtil {
	private static final String HTTP_LOCALHOST_9080_WEBSERVICE_SCG_SCG_SERVICE = "http://localhost:9080/webservice-scg/SCGService";

	public static void startMessages() {
		System.out.println("---------------started----------");
		System.out.println("Service Running on: " + HTTP_LOCALHOST_9080_WEBSERVICE_SCG_SCG_SERVICE);
		System.out.println(HTTP_LOCALHOST_9080_WEBSERVICE_SCG_SCG_SERVICE + "?wsdl");
	}

	public static void resquestMessages(String fileContent) {
		System.out.println("\n Requisição recebida as..." + new Date());
		System.out.println("-----------Inicio do conteúdo -----------");
		System.out.println(fileContent);
		System.out.println("----------Término do conteúdo -----------");
	}

}
