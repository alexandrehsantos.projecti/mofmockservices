package br.com.projecti.mockservices.resource;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.projecti.mockservices.dto.JsonDinamicoDTO;

@RestController
public class AutenticationResource {

	private static final String SECRET_VALUE = "50A77B7A1CB9D527CAAE8A04C36257D5CF0C86328FFB43B9AC51B73435A2343F";
	private static final String KEY_ID = "[B@dd5a70";
	private static final String ACCESS_TYPE = "access_type";
	private static final String TOKEN_FIXO = "e4fbd53c-54ce-3cc0-8af8-4c32f74be7f5";

	@PostMapping
	@RequestMapping("/user/authentication")
	public ResponseEntity getAccessToken(@RequestBody JsonDinamicoDTO jsonDinamicoDTO) {

		Map<String, String> jsonEntrada = jsonDinamicoDTO.getJsonDinamico();
		String keyIdValue = jsonEntrada.get("keyId");
		String keySecretValue = jsonEntrada.get("keySecret");

		if (!keyIdValue.equals(KEY_ID) || !keySecretValue.equals(SECRET_VALUE)) {
			return new ResponseEntity<JsonDinamicoDTO>(HttpStatus.UNAUTHORIZED);
		}

		JsonDinamicoDTO jsonDinamicoDTO2 = new JsonDinamicoDTO();
		Map<String, String> jsonDinamic = new HashMap<>();

		jsonDinamic.put("access_token", TOKEN_FIXO);
		jsonDinamic.put("token_type", ACCESS_TYPE);
		jsonDinamic.put("expires_in", "3600");

		jsonDinamicoDTO2.jsonDinamico(jsonDinamic);

		return ResponseEntity.ok(jsonDinamicoDTO2);

	}


}
