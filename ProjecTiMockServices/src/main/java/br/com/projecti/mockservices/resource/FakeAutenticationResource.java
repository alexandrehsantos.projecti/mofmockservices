package br.com.projecti.mockservices.resource;

import java.io.IOException;

import javax.xml.ws.WebServiceException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.projecti.mockservices.dto.FakeTokenDTO;

@RestController
// @RequestMapping("/mock")
public class FakeAutenticationResource {

	private static final String USER_AGENT = "Windows NT";
	private static final String TOKEN_FIXO = "e4fbd53c-54ce-3cc0-8af8-4c32f74be7f5";
	private static final String JSON_VALUE = "\"client_credentials\"";
	private static final String CLIENT_ID = "3bd1b334-bdcc-11e8-a355-529269fb1459";
	private static final String AUTORIZATION_TOKEN = "Basic M2JkMWIzMzQtYmRjYy0xMWU4LWEzNTUtNTI5MjY5ZmIxNDU5OjcxYTdjNjEwLWJkY2MtMTFlOC1hMzU1LTUyOTI2OWZiMTQ1OQ==";
	private static final String DEFAULT_1 = "application/json";

	@PostMapping
	@RequestMapping("/oauth/access-token")
	public ResponseEntity<FakeTokenDTO> getAccessToken(@RequestHeader("Content-Type") String defaultHeader1,
			@RequestHeader("Accept") String defaultHeader2, @RequestHeader("Authorization") String tokenAutorization,
			@RequestHeader("client_id") String clienId, @RequestHeader("User-Agent") String userAgent,
			@RequestBody String grant_type) throws JsonProcessingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode readTree = objectMapper.readTree(grant_type);
		JsonNode jsonNode = null;
		if (userAgent != null) {
			jsonNode = readTree.get("grant_type");
		} else {
			throw new WebServiceException("parametro de entrada inválidos: Json de entrada não informado");
		}

		if (defaultHeader1.equals(DEFAULT_1) && defaultHeader2.equals(DEFAULT_1)
				&& tokenAutorization.equals(AUTORIZATION_TOKEN) && clienId.equals(CLIENT_ID)
				&& userAgent.contains(USER_AGENT) && jsonNode.toString().equals(JSON_VALUE)) {
			String token = TOKEN_FIXO;

			FakeTokenDTO fakeTokenDTO = new FakeTokenDTO();
			fakeTokenDTO.setAccess_token(token);
			fakeTokenDTO.setToken_type("access_type");
			fakeTokenDTO.setExpires_in("3600");

			return new ResponseEntity<>(fakeTokenDTO, HttpStatus.CREATED);
		} else {
			throw new WebServiceException("parametro de entrada inválidos");
		}

		// SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
		// MessageDigest digest = MessageDigest.getInstance("SHA-256");
		// secureRandom.setSeed(secureRandom.generateSeed(128));
		// String keysource = new String(digest.digest((secureRandom.nextLong() +
		// "").getBytes()));
		//
		// byte [] tokenByte = new Base64(true).encodeBase64(keysource.getBytes());
		// String token = new String(tokenByte);

		// Random random = new Random();
		// long token = random.nextLong();
		//
		// return token;
	}

}
