package br.com.projecti.mockservices.resource;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projecti.mockservices.dao.MockRestDao;
import br.com.projecti.mockservices.entity.MockRestJson;

@RestController
public class NotificacaoVinculacaoResource {
	@Autowired
	private MockRestDao mockdao;

	@RequestMapping(value = "/notificacao-vinculacao/enviaPacoteElegivelConflitante", method = RequestMethod.POST)
	public ResponseEntity enviaPacoteElegivelConflitante(@RequestBody String values) {
		System.out.println(values);

		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setCallerDescription(
				"Serviço de Notificação Vinculação, enviando pacotes elegiveis mais pacotes desvinculados");
		mockRestJson.setJson(values);

		mockRestJson.setUrlMock("/notificacao-vinculacao/enviaPacoteElegivelConflitante");
		mockdao.save(mockRestJson);

		return ResponseEntity.ok(values);
	}

	@RequestMapping(value = "/notificacao-vinculacao/enviaPacoteElegivel", method = RequestMethod.POST)
	public ResponseEntity enviaPacoteElegivel(@RequestBody String values) {
		System.out.println(values);

		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setCallerDescription("Serviço de Notificação Vinculação, enviando pacotes elegiveis");
		mockRestJson.setJson(values);

		mockRestJson.setUrlMock("/notificacao-vinculacao/enviaPacoteElegivel");
		mockdao.save(mockRestJson);

		return ResponseEntity.ok(values);
	}

}
