package br.com.projecti.mockservices.resource;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DescontoProgressivo {

	@GetMapping("/trp/corpclienteapi/estabelecimento/descontoprogressivo/{codigoEC}/valorbase")
	public ResponseEntity getValorBase(@PathVariable Long codigoEC) {
		Map<Object, Object> jsonDinamic = new HashMap<>();
		jsonDinamic.put("valorDescontoProgressivo", new BigDecimal(19.99));
		return ResponseEntity.ok(jsonDinamic);
	}

}