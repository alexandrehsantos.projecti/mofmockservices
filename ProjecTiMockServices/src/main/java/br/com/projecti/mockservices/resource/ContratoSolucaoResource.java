package br.com.projecti.mockservices.resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import br.com.projecti.mockservices.dao.MockRestDao;
import br.com.projecti.mockservices.entity.MockRestJson;

@RestController
public class ContratoSolucaoResource {

	@Autowired
	private MockRestDao mockdao;

	@PutMapping("/contrato-solucao/cancelar")
	public ResponseEntity cancelar(@RequestBody String jsonDinamicoDTO) {

		JsonObject objeto1 = new JsonObject();
		// HashMap<Object, Object> objeto1 = new HashMap<>();
		objeto1.addProperty("codigo", 123);
		objeto1.addProperty("codigoCliente", 22);
		objeto1.addProperty("codigoSolicitacao", 11);
		objeto1.addProperty("codigoSolucao", 33);
		objeto1.addProperty("codigoTipoEquipamento", 1);
		objeto1.addProperty("dataContratacao", "2019-01-08T15:31:41.909Z");
		objeto1.addProperty("dataDesinstalacao", "2019-01-08T15:31:41.909Z");
		objeto1.addProperty("dataInstalacao", "2019-01-08T15:31:41.909Z");
		objeto1.addProperty("descricaoAliasSistemas", "AQDMOF");
		objeto1.addProperty("flagAtivo", "S");
		objeto1.addProperty("flagNegEspecial", "S");
		objeto1.addProperty("numeroChamadoDesinstalacao", 1);
		objeto1.addProperty("numeroChamadoDInstalacao", 2);
		objeto1.addProperty("numeroMeses", 12);
		objeto1.addProperty("numeroPatrimonio", 0);
		objeto1.addProperty("numeroSerie", 1234);
		objeto1.addProperty("tarifaUnica", true);
		objeto1.addProperty("valorNegocioEspecial", new BigDecimal(20.0));
		objeto1.addProperty("valorPadrao", new BigDecimal(20.0));
		objeto1.addProperty("valorParcelaNegocioEspecial", new BigDecimal(20.0));
		System.out.println(objeto1.toString());
		String json = objeto1.toString();

		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setCallerDescription("Serviço Atualização valor equipamento -CANCELAR");
		mockRestJson.setJson(jsonDinamicoDTO);

		mockRestJson.setUrlMock("/contrato-solucao/cancelar");
		mockdao.save(mockRestJson);

		return ResponseEntity.ok(objeto1.toString());
	}

	@PostMapping("/contrato-solucao/contratar")
	public ResponseEntity contratar(@RequestBody String jsonDinamicoDTO) {
		Map<Object, Object> objeto1 = new HashMap<>();
		objeto1.put("contrato-solucao", 123);
		ArrayList<Map<Object, Object>> retorno = new ArrayList<>();
		retorno.add(objeto1);

		String json = new Gson().toJson(retorno);
		System.out.println(json);
		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setCallerDescription("Serviço Atualização valor equipamento -CONTRATAR");
		mockRestJson.setJson(jsonDinamicoDTO);

		mockRestJson.setUrlMock("/contrato-solucao/contratar");
		mockdao.save(mockRestJson);
		return new ResponseEntity(json, HttpStatus.CREATED);
	}

	@PutMapping("/contrato-solucao/ativar")
	public ResponseEntity ativar(@RequestBody String jsonDinamicoDTO) {

		JsonObject objeto1 = new JsonObject();
		objeto1.addProperty("codigo", 123);
		objeto1.addProperty("codigoCliente", 22);
		objeto1.addProperty("codigoSolicitacao", 11);
		objeto1.addProperty("codigoSolucao", 33);
		objeto1.addProperty("codigoTipoEquipamento", 1);
		objeto1.addProperty("dataContratacao", "2019-01-08T15:31:41.909Z");
		objeto1.addProperty("dataDesinstalacao", "2019-01-08T15:31:41.909Z");
		objeto1.addProperty("dataInstalacao", "2019-01-08T15:31:41.909Z");
		objeto1.addProperty("descricaoAliasSistemas", "AQDMOF");
		objeto1.addProperty("flagAtivo", "S");
		objeto1.addProperty("flagNegEspecial", "S");
		objeto1.addProperty("numeroChamadoDesinstalacao", 1);
		objeto1.addProperty("numeroChamadoDInstalacao", 2);
		objeto1.addProperty("numeroMeses", 12);
		objeto1.addProperty("numeroPatrimonio", 0);
		objeto1.addProperty("numeroSerie", 1234);
		objeto1.addProperty("tarifaUnica", true);
		objeto1.addProperty("valorNegocioEspecial", new BigDecimal(20.0));
		objeto1.addProperty("valorPadrao", new BigDecimal(20.0));
		objeto1.addProperty("valorParcelaNegocioEspecial", new BigDecimal(20.0));

		System.out.println(objeto1.toString());
		String json = objeto1.toString();

		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setCallerDescription("Servi�o Atualiza��o valor equipamento -ATIVAR");
		mockRestJson.setJson(jsonDinamicoDTO);

		mockRestJson.setUrlMock("/contrato-solucao/ativar");
		mockdao.save(mockRestJson);

		return ResponseEntity.ok(objeto1.toString());
	}

	private int getRandomId() {
		Random r = new Random();
		return r.ints(1, (10000000 + 1)).findFirst().getAsInt();
	}

}
