package br.com.projecti.mockservices.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestTest {

	@RequestMapping(value = "/casemanual/new", method = RequestMethod.POST)
	public void testGet(
						@RequestPart("idOperacao") String idOperacao,
						@RequestPart("idTipoCaso") String idTipoCaso,
						@RequestPart("idUsuario") String idUsuario,
						@RequestPart("idEvento") String idEvento,
						@RequestPart("idCanal") String idCanal,
						@RequestPart("observacao") String observacao,
						@RequestPart("codigoEc") String codigoEc, 
						@RequestPart("cnpjECommonFileServiceImplTestc") String cnpjEC
			) {
		
		
		System.out.println("Recendo Requisicao em /casemanual/new");
		System.out.println("parametros: ");
		System.out.println("idOperacao: " + idOperacao);
		System.out.println("idTipoCaseTeamTask: " + idTipoCaso);
		System.out.println("idUsuario: " + idUsuario);
		System.out.println("idEvento: " + idEvento);
		System.out.println("idCanal: " + idCanal);
		System.out.println("observacao: " + observacao); 
		System.out.println("codigoEc: " + codigoEc);
		System.out.println("cnpjEc: " + cnpjEC);
		
		
	}

}
