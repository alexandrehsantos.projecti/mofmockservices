//package br.com.projecti.mockservices.resource;
//
//import java.io.FileNotFoundException;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import br.com.projecti.mockservices.entity.MockRestJson;
//
//@RestController
//@RequestMapping("/bfc-tbo/clientes")
//public class ConcessaoBeneficio {
//
//	@Autowired
//	private MockRestDao mockRestDao;
//
//	@Value(value = "${json.mock.path.concessao_beneficio}")
//	private String fileJsonConcessao;
//
//	@Value(value = "${json.mock.path.suspensao_beneficio}")
//	private String fileJsonSuspensao;
//
//	@PostMapping("/inserir")
//	public ResponseEntity conceder(@RequestBody String jsonDinamicoDTO) throws FileNotFoundException {
//
//		MockRestJson mockRestJson = new MockRestJson();
//
//		mockRestJson.setUrlMock("/bfc-tbo/clientes/inserir");
//		mockRestJson.setCallerDescription("Serviço utilizado para conceder um único benefico");
//		mockRestJson.setJson(jsonDinamicoDTO.toString());
//
//		mockRestDao.save(mockRestJson);
//		String jsonString = MockUtil.getFileFromPath(fileJsonConcessao);
//
//		return ResponseEntity.ok(jsonString);
//	}
//
//	@PostMapping("/excluir")
//	public ResponseEntity suspender(@RequestBody String jsonDinamicoDTO) throws FileNotFoundException {
//
//		MockRestJson mockRestJson = new MockRestJson();
//
//		mockRestJson.setUrlMock("/bfc-tbo/clientes/inserir");
//		mockRestJson.setCallerDescription("Serviço utilizado para conceder um único benefico");
//		mockRestJson.setJson(jsonDinamicoDTO.toString());
//
//		mockRestDao.save(mockRestJson);
//		String jsonString = MockUtil.getFileFromPath(fileJsonSuspensao);
//
//		return ResponseEntity.ok(jsonString);
//	}
//
//}
