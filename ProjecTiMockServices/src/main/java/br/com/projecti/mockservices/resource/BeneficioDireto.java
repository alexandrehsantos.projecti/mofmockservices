package br.com.projecti.mockservices.resource;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("trp/corpclienteapi")
public class BeneficioDireto {

	@GetMapping("/estabelecimento/tarifa-unica/{codigoEC}")
	public ResponseEntity getBeneficioDireto(@PathVariable Long codigoEC) {
		Map<Object, Object> jsonValue = new HashMap<>();
		jsonValue.put("valorCondicaoUnica", new BigDecimal(200.00));
		return ResponseEntity.ok(jsonValue);
	}

	@GetMapping("/contrato-solucao/paginada/params?codigoEstabelecimento={codigoEC}&codigoTipoEquipamento={tipoEquipamento}&flagNegEspecial={flagNegEspecial}&pagina={pagina}&linhasPorPagina={linhas}&orderBy=codigo&direcao=ASC")
	public ResponseEntity getEquipamentoList(Long codigoEC) {

		return null;
	}

}