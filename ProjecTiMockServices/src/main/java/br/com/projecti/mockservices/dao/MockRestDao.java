package br.com.projecti.mockservices.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.projecti.mockservices.entity.MockRestJson;

@Repository
public interface MockRestDao  extends JpaRepository<MockRestJson,Long> {
	
}
