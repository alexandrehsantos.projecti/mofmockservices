package br.com.projecti.mockservices.dto;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections4.MapUtils;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonDinamicoDTO {

	private Map<String, String> jsonDinamico = new HashMap<>();

	@JsonAnySetter
	public void handleUnknownProperty(String key, String value) {
		getJsonDinamico().put(key, value);
	}

	/**
	 * @return the jsonDinamic
	 */
	public Map<String, String> getJsonDinamico() {
		return jsonDinamico;
	}

	/**
	 * @param jsonDinamico
	 *            the jsonDinamic to set
	 */
	public void jsonDinamico(Map<String, String> jsonDinamico) {
		this.jsonDinamico = jsonDinamico;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder jsonBuid = new StringBuilder();

		if (MapUtils.isNotEmpty(jsonDinamico)) {
			jsonBuid.append("{");
			Set<Entry<String, String>> entrySet = jsonDinamico.entrySet();
			for (Entry<String, String> entry : entrySet) {

				jsonBuid.append("" + entry.getKey() + "\"" + ":");
				jsonBuid.append("\"" + entry.getValue() + "\"" + ",");

			}
			jsonBuid.append("}");

		}

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		return gson.toJson(jsonBuid);
	}

}