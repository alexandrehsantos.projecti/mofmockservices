package br.com.projecti.mockservices.resource;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.projecti.mockservices.dao.MockRestDao;
import br.com.projecti.mockservices.entity.MockRestJson;

@RestController
@RequestMapping("/sandbox/v1/trp/corpdadosauxiliaresapi/buscar-cnae-mcc")
public class MccResource {

	@Autowired
	private MockRestDao mockdao;

	@GetMapping
	public ResponseEntity getList(@RequestParam(value = "nroCNAE", required = true) String nroCNAE) {

		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setCallerDescription("paratros: nroCNAE = " + nroCNAE);
		mockRestJson.setDataCriacao(new Date());
		mockdao.save(mockRestJson);

		return ResponseEntity.ok(buildJson());

	}

	private String buildJson() {
		return "[\r\n" + "   {\r\n" + "       \"mcc\": 5912,\r\n" + "       \"irs\": \"Drogarias e Farmÿcias\",\r\n"
				+ "       \"cnae\": 4771701,\r\n"
				+ "       \"ramoAtividade\": \"Comércio varejista de produtos farmacêuticos, sem manipulação de fórmulas\"\r\n"
				+ "   }\r\n" + "]";
	}

}
