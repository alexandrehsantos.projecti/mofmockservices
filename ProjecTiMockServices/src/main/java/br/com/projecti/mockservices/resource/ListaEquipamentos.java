package br.com.projecti.mockservices.resource;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.projecti.mockservices.dao.MockRestDao;
import br.com.projecti.mockservices.dto.Contratacao;
import br.com.projecti.mockservices.dto.Detalhe;
import br.com.projecti.mockservices.dto.Equipamento;
import br.com.projecti.mockservices.dto.Estabelecimento;
import br.com.projecti.mockservices.dto.Financeiro;
import br.com.projecti.mockservices.dto.Solucao;
import br.com.projecti.mockservices.entity.MockRestJson;

@RestController
@RequestMapping("/contrato-solucao/paginada/params")
public class ListaEquipamentos {

	@Autowired
	private MockRestDao mockdao;

	@GetMapping
	public ResponseEntity getList(
			@RequestParam(value = "codigoEstabelecimento", required = false) String codigoEstabelecimento,
			@RequestParam(value = "codigoSolucao", required = false) String codigoSolucao,
			@RequestParam(value = "flagAtivo", required = false) String flagAtivo,
			@RequestParam(value = "pagina", required = false) Integer pagina,
			@RequestParam(value = "linhas", required = false) Integer linhas,
			@RequestParam(value = "orderBy", required = false) String orderBy,
			@RequestParam(value = "direcao", required = false) String direcao, Pageable pageable

	) {

		StringBuilder parametrosRecebidos = new StringBuilder();
		parametrosRecebidos.append("Parametros recebidos: ");
		parametrosRecebidos.append(" codigoEstabelecimento: " + codigoEstabelecimento);
		parametrosRecebidos.append(" codigoSolucao: " + codigoSolucao);
		parametrosRecebidos.append(" flagAtivo: " + flagAtivo);
		parametrosRecebidos.append(" pagina: " + pagina);
		parametrosRecebidos.append(" linhas: " + linhas);
		parametrosRecebidos.append(" orderBy: " + orderBy);
		parametrosRecebidos.append(" direcao: " + direcao);
		parametrosRecebidos.append(" pageable : " + pageable);

		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setCallerDescription(parametrosRecebidos.toString());
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setUrlMock("/contrato-solucao/paginada/params");

		mockdao.save(mockRestJson);

		// Retorno buildRetorno = buildRetorno();

		return ResponseEntity.ok(buildJson());

	}

	private String buildJson() {

		return "{\r\n" + "   \"contratacoes\":[\r\n" + "      {\r\n" + "         \"estabelecimento\":{\r\n"
				+ "            \"codigo\":50044,\r\n" + "            \"equipamento\":{\r\n"
				+ "               \"codigo_contratacao\":2434,\r\n" + "               \"id_tipo_equipamento\":1,\r\n"
				+ "               \"id_solucao\":2,\r\n" + "               \"tipo_solucao\":\"POS COM FIO\",\r\n"
				+ "               \"descricao_alias_sistemas\":\"PORTAL\",\r\n"
				+ "               \"data_contratacao\":\"2019-02-13 17:28:03.281\",\r\n"
				+ "               \"solicitacoes\":[\r\n" + "\r\n" + "               ],\r\n"
				+ "               \"detalhes\":{\r\n" + "\r\n" + "               },\r\n"
				+ "               \"financeiro\":{\r\n" + "                  \"valor_padrao\":150.0,\r\n"
				+ "                  \"valor_aluguel_negociacao\":133.5,\r\n"
				+ "                  \"data_negociacao\":\"2019-03-13 00:00:00.00\",\r\n"
				+ "                  \"percentual_desconto\":11.0,\r\n" + "                  \"meses_carencia\":1,\r\n"
				+ "                  \"valor_desconto\":133.5,\r\n" + "                  \"tarifa_unica\":\"N\"\r\n"
				+ "               }\r\n" + "            }\r\n" + "         },\r\n" + "         \"solucoes\":{\r\n"
				+ "            \"tipo_id\":2,\r\n" + "            \"tipo\":\"POS COM FIO\",\r\n"
				+ "            \"data_contratacao\":\"2019-02-13 17:28:03.281\",\r\n"
				+ "            \"financeiro\":{\r\n" + "               \"valor_padrao\":150.0,\r\n"
				+ "               \"percentual_desconto\":11.0,\r\n" + "               \"meses_carencia\":1,\r\n"
				+ "               \"valor_desconto\":133.5\r\n" + "            }\r\n" + "         }\r\n" + "      }\r\n"
				+ "   ],\r\n" + "   \"last\":true,\r\n" + "   \"first\":true,\r\n" + "   \"sort\":[\r\n" + "      {\r\n"
				+ "         \"direction\":\"ASC\",\r\n" + "         \"property\":\"dataInstalacao\",\r\n"
				+ "         \"ignoreCase\":false,\r\n" + "         \"nullHandling\":\"NATIVE\",\r\n"
				+ "         \"ascending\":true,\r\n" + "         \"descending\":false\r\n" + "      }\r\n" + "   ],\r\n"
				+ "   \"numberOfElements\":1,\r\n" + "   \"totalPages\":1,\r\n" + "   \"totalElements\":1,\r\n"
				+ "   \"size\":1,\r\n" + "   \"number\":0\r\n" + "}";

		// retorno antigo
		// return "{\r\n" +
		// " \"content\":[\r\n" +
		// " {\r\n" +
		// " \"content\":[\r\n" +
		// " null\r\n" +
		// " ],\r\n" +
		// " \"contratacoes\":[\r\n" +
		// " {\r\n" +
		// " \"estabelecimento\":{\r\n" +
		// " \"codigo\":0,\r\n" +
		// " \"equipamento\":{\r\n" +
		// " \"codigoContratoSolucao\":2,\r\n" +
		// " \"codigo_contratacao\":0,\r\n" +
		// " \"data_contratacao\":\"2019-01-09 10:44:15.301\",\r\n" +
		// " \"data_desinstalacao\":\"string\",\r\n" +
		// " \"data_instalacao\":\"string\",\r\n" +
		// " \"data_solicitacao_cancelamento\":\"string\",\r\n" +
		// " \"descricao_alias_sistemas\":\"string\",\r\n" +
		// " \"detalhes\":{\r\n" +
		// " \"conectividade\":{\r\n" +
		// " \"imei_chip\":\"string\",\r\n" +
		// " \"operadora\":\"string\"\r\n" +
		// " },\r\n" +
		// " \"id_equipamento\":0,\r\n" +
		// " \"modelo\":\"string\",\r\n" +
		// " \"patrimonio\":\"string\",\r\n" +
		// " \"status\":\"string\"\r\n" +
		// " },\r\n" +
		// " \"financeiro\":{\r\n" +
		// " \"meses_carencia\":0,\r\n" +
		// " \"percentual_desconto\":0,\r\n" +
		// " \"valor_desconto\":0,\r\n" +
		// " \"valor_padrao\":15\r\n" +
		// " },\r\n" +
		// " \"id_solucao\":0,\r\n" +
		// " \"id_tipo_equipamento\":0,\r\n" +
		// " \"numero_patrimonio\":0,\r\n" +
		// " \"patrimonio\":\"string\",\r\n" +
		// " \"serie\":0,\r\n" +
		// " \"solicitacoes\":[\r\n" +
		// " {\r\n" +
		// " \"chamado_desinstalacao\":0,\r\n" +
		// " \"chamado_instalacao\":0,\r\n" +
		// " \"os_desinstalacao\":\"string\",\r\n" +
		// " \"os_instalacao\":\"string\"\r\n" +
		// " }\r\n" +
		// " ],\r\n" +
		// " \"tipo_solucao\":\"string\",\r\n" +
		// " \"idTipo\":0,\r\n" +
		// " \"tipo\":\"string\",\r\n" +
		// " \"valorAluguel\":0,\r\n" +
		// " \"valorAluguelNegociacao\":0,\r\n" +
		// " \"quantidade\":0\r\n" +
		// " }\r\n" +
		// " },\r\n" +
		// " \"solucoes\":{\r\n" +
		// " \"data_ativacao\":\"string\",\r\n" +
		// " \"data_contratacao\":\"2019-01-09 22:35:30.414\",\r\n" +
		// " \"data_desinstalacao\":\"string\",\r\n" +
		// " \"financeiro\":{\r\n" +
		// " \"meses_carencia\":0,\r\n" +
		// " \"percentual_desconto\":0,\r\n" +
		// " \"valor_desconto\":0,\r\n" +
		// " \"valor_padrao\":0\r\n" +
		// " },\r\n" +
		// " \"tipo\":\"string\",\r\n" +
		// " \"tipo_id\":0\r\n" +
		// " }\r\n" +
		// " },\r\n" +
		// " {\r\n" +
		// " \"estabelecimento\":{\r\n" +
		// " \"codigo\":0,\r\n" +
		// " \"equipamento\":{\r\n" +
		// " \"codigoContratoSolucao\":4,\r\n" +
		// " \"codigo_contratacao\":0,\r\n" +
		// " \"data_contratacao\":\"2019-01-11 10:44:15.301\",\r\n" +
		// " \"data_desinstalacao\":\"string\",\r\n" +
		// " \"data_instalacao\":\"string\",\r\n" +
		// " \"data_solicitacao_cancelamento\":\"string\",\r\n" +
		// " \"descricao_alias_sistemas\":\"string\",\r\n" +
		// " \"detalhes\":{\r\n" +
		// " \"conectividade\":{\r\n" +
		// " \"imei_chip\":\"string\",\r\n" +
		// " \"operadora\":\"string\"\r\n" +
		// " },\r\n" +
		// " \"id_equipamento\":0,\r\n" +
		// " \"modelo\":\"string\",\r\n" +
		// " \"patrimonio\":\"string\",\r\n" +
		// " \"status\":\"string\"\r\n" +
		// " },\r\n" +
		// " \"financeiro\":{\r\n" +
		// " \"meses_carencia\":0,\r\n" +
		// " \"percentual_desconto\":0,\r\n" +
		// " \"valor_desconto\":0,\r\n" +
		// " \"valor_padrao\":50\r\n" +
		// " },\r\n" +
		// " \"id_solucao\":0,\r\n" +
		// " \"id_tipo_equipamento\":0,\r\n" +
		// " \"numero_patrimonio\":0,\r\n" +
		// " \"patrimonio\":\"string\",\r\n" +
		// " \"serie\":0,\r\n" +
		// " \"solicitacoes\":[\r\n" +
		// " {\r\n" +
		// " \"chamado_desinstalacao\":0,\r\n" +
		// " \"chamado_instalacao\":0,\r\n" +
		// " \"os_desinstalacao\":\"string\",\r\n" +
		// " \"os_instalacao\":\"string\"\r\n" +
		// " }\r\n" +
		// " ],\r\n" +
		// " \"tipo_solucao\":\"string\",\r\n" +
		// " \"idTipo\":0,\r\n" +
		// " \"tipo\":\"string\",\r\n" +
		// " \"valorAluguel\":0,\r\n" +
		// " \"valorAluguelNegociacao\":0,\r\n" +
		// " \"quantidade\":0\r\n" +
		// " }\r\n" +
		// " },\r\n" +
		// " \"solucoes\":{\r\n" +
		// " \"data_ativacao\":\"string\",\r\n" +
		// " \"data_contratacao\":\"2019-01-09 22:35:30.414\",\r\n" +
		// " \"data_desinstalacao\":\"string\",\r\n" +
		// " \"financeiro\":{\r\n" +
		// " \"meses_carencia\":0,\r\n" +
		// " \"percentual_desconto\":0,\r\n" +
		// " \"valor_desconto\":0,\r\n" +
		// " \"valor_padrao\":0\r\n" +
		// " },\r\n" +
		// " \"tipo\":\"string\",\r\n" +
		// " \"tipo_id\":0\r\n" +
		// " }\r\n" +
		// " },\r\n" +
		// " {\r\n" +
		// " \"estabelecimento\":{\r\n" +
		// " \"codigo\":0,\r\n" +
		// " \"equipamento\":{\r\n" +
		// " \"codigoContratoSolucao\":3,\r\n" +
		// " \"codigo_contratacao\":0,\r\n" +
		// " \"data_contratacao\":\"2019-01-10 10:44:15.301\",\r\n" +
		// " \"data_desinstalacao\":\"string\",\r\n" +
		// " \"data_instalacao\":\"string\",\r\n" +
		// " \"data_solicitacao_cancelamento\":\"string\",\r\n" +
		// " \"descricao_alias_sistemas\":\"string\",\r\n" +
		// " \"detalhes\":{\r\n" +
		// " \"conectividade\":{\r\n" +
		// " \"imei_chip\":\"string\",\r\n" +
		// " \"operadora\":\"string\"\r\n" +
		// " },\r\n" +
		// " \"id_equipamento\":0,\r\n" +
		// " \"modelo\":\"string\",\r\n" +
		// " \"patrimonio\":\"string\",\r\n" +
		// " \"status\":\"string\"\r\n" +
		// " },\r\n" +
		// " \"financeiro\":{\r\n" +
		// " \"meses_carencia\":0,\r\n" +
		// " \"percentual_desconto\":0,\r\n" +
		// " \"valor_desconto\":0,\r\n" +
		// " \"valor_padrao\":20\r\n" +
		// " },\r\n" +
		// " \"id_solucao\":0,\r\n" +
		// " \"id_tipo_equipamento\":0,\r\n" +
		// " \"numero_patrimonio\":0,\r\n" +
		// " \"patrimonio\":\"string\",\r\n" +
		// " \"serie\":0,\r\n" +
		// " \"solicitacoes\":[\r\n" +
		// " {\r\n" +
		// " \"chamado_desinstalacao\":0,\r\n" +
		// " \"chamado_instalacao\":0,\r\n" +
		// " \"os_desinstalacao\":\"string\",\r\n" +
		// " \"os_instalacao\":\"string\"\r\n" +
		// " }\r\n" +
		// " ],\r\n" +
		// " \"tipo_solucao\":\"string\",\r\n" +
		// " \"idTipo\":0,\r\n" +
		// " \"tipo\":\"string\",\r\n" +
		// " \"valorAluguel\":0,\r\n" +
		// " \"valorAluguelNegociacao\":0,\r\n" +
		// " \"quantidade\":0\r\n" +
		// " }\r\n" +
		// " },\r\n" +
		// " \"solucoes\":{\r\n" +
		// " \"data_ativacao\":\"string\",\r\n" +
		// " \"data_contratacao\":\"2019-01-09 22:35:30.414\",\r\n" +
		// " \"data_desinstalacao\":\"string\",\r\n" +
		// " \"financeiro\":{\r\n" +
		// " \"meses_carencia\":0,\r\n" +
		// " \"percentual_desconto\":0,\r\n" +
		// " \"valor_desconto\":0,\r\n" +
		// " \"valor_padrao\":0\r\n" +
		// " },\r\n" +
		// " \"tipo\":\"string\",\r\n" +
		// " \"tipo_id\":0\r\n" +
		// " }\r\n" +
		// " }\r\n" +
		// " ],\r\n" +
		// " \"first\":true,\r\n" +
		// " \"last\":true,\r\n" +
		// " \"number\":0,\r\n" +
		// " \"numberOfElements\":0,\r\n" +
		// " \"size\":0,\r\n" +
		// " \"sort\":{\r\n" +
		// "\r\n" +
		// " },\r\n" +
		// " \"totalElements\":0,\r\n" +
		// " \"totalPages\":0\r\n" +
		// " }\r\n" +
		// " ],\r\n" +
		// " \"first\":true,\r\n" +
		// " \"last\":true,\r\n" +
		// " \"number\":0,\r\n" +
		// " \"numberOfElements\":0,\r\n" +
		// " \"size\":0,\r\n" +
		// " \"sort\":{\r\n" +
		// "\r\n" +
		// " },\r\n" +
		// " \"totalElements\":0,\r\n" +
		// " \"totalPages\":0\r\n" +
		// "}";
	}

	private Retorno buildRetorno() {

		Retorno retorno = new Retorno();

		ContentDto contentDto = new ContentDto();
		contentDto.setContent(null);
		List<ContentDto> contentDtos = new ArrayList<>();
		contentDtos.add(contentDto);

		ContentRootDto contentRootDto = new ContentRootDto();
		contentRootDto.setContent(contentDtos);

		retorno.setContent(contentRootDto);

		List<Contratacao> contratacoes = new ArrayList<>();

		Contratacao contratacao = new Contratacao();
		Estabelecimento estabelecimento = new Estabelecimento();
		estabelecimento.setCodigo(1L);
		Equipamento equipamentos = new Equipamento();
		equipamentos.setCodigoContratoSolucao(2L);
		equipamentos.setCodigo_contratacao(26L);
		equipamentos.setId_tipo_equipamento(1L);
		equipamentos.setId_solucao(2L);
		equipamentos.setTipo_solucao("POS COM FIO");
		equipamentos.setDescricao_alias_sistemas("CREDENCIAMENTO");
		equipamentos.setData_contratacao(LocalDateTime.now());
		Detalhe detalhes = new Detalhe();
		equipamentos.setDetalhes(detalhes);
		Financeiro financeiro = new Financeiro();
		financeiro.setValor_padrao(Double.valueOf(15));
		financeiro.setTarifa_unica("N");
		equipamentos.setFinanceiro(financeiro);
		estabelecimento.setEquipamento(equipamentos);

		contratacao.setEstabelecimento(estabelecimento);

		Solucao solucao = new Solucao();
		solucao.setTipo_id(2);
		solucao.setTipo("POS COM FIO");
		solucao.setData_contratacao(LocalDateTime.now());
		solucao.setFinanceiro(financeiro);

		contratacao.setSolucao(solucao);

		contratacoes.add(contratacao);

		retorno.setContratacoes(contratacoes);

		retorno.setLast(true);
		Sort e = new Sort();
		e.setAscending(true);
		e.setDirection("ASC");
		e.setProperty("codigo");
		e.setIgnoreCase(false);
		e.setNullHandling("NATIVE");
		e.setDescending(false);

		retorno.getSort().add(e);

		return retorno;
	}

}

class Retorno {

	ContentRootDto content;
	List<Contratacao> contratacoes;
	boolean last = true;
	List<Sort> sort = new ArrayList<>();

	public List<Contratacao> getContratacoes() {
		return contratacoes;
	}

	public boolean isLast() {
		return last;
	}

	public List<Sort> getSort() {
		return sort;
	}

	public void setContratacoes(List<Contratacao> contratacoes) {
		this.contratacoes = contratacoes;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public void setSort(List<Sort> sort) {
		this.sort = sort;
	}

	public void setContent(ContentRootDto content) {
		this.content = content;
	}

}

// params?codigoEstabelecimento={codigoEC}&codigoTipoEquipamento={tipoEquipamento}&flagNegEspecial={flagNegEspecial}&pagina={pagina}&linhasPorPagina={linhas}&orderBy=codigo&direcao=ASC
