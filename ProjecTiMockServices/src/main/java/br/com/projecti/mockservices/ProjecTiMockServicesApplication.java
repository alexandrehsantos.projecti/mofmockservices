package br.com.projecti.mockservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjecTiMockServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjecTiMockServicesApplication.class, args);
	}

}
