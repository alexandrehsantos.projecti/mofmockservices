package br.com.projecti.mockservices.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Solucao {

	private Integer tipo_id;
	private String tipo;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
	private LocalDateTime data_contratacao;
	private Financeiro financeiro;

	public Integer getTipo_id() {
		return tipo_id;
	}

	public LocalDateTime getData_contratacao() {
		return data_contratacao;
	}

	public Financeiro getFinanceiro() {
		return financeiro;
	}

	public void setTipo_id(Integer tipo_id) {
		this.tipo_id = tipo_id;
	}

	public void setData_contratacao(LocalDateTime localDateTime) {
		this.data_contratacao = localDateTime;
	}

	public void setFinanceiro(Financeiro financeiro) {
		this.financeiro = financeiro;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
