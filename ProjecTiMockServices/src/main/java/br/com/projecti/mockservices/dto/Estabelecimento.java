package br.com.projecti.mockservices.dto;

public class Estabelecimento {

	private Long codigo;
	private Equipamento equipamento;

	public Equipamento getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(Equipamento equipamentos) {
		this.equipamento = equipamentos;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

}
