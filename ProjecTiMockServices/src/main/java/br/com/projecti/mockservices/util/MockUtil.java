package br.com.projecti.mockservices.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;

public class MockUtil {

	public static String getFileFromPath(String fileName) throws FileNotFoundException {
		StringBuilder content = new StringBuilder();
		File file = new File(fileName);
		if (file.exists()) {
			try (FileReader fileReader = new FileReader(file)) {
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String str = null;
				while ((str = bufferedReader.readLine()) != null) {
					content.append(str);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return content.toString();
	}

}
	