package br.com.projecti.mockservices.resource;

public class Sort {

	private String direction = "ASC";
	private String property;
	private boolean ignoreCase = false;
	private String nullHandling = "NATIVE";
	private boolean ascending = true;
	private boolean descending = false;

	public String getDirection() {
		return direction;
	}

	public String getProperty() {
		return property;
	}

	public boolean isIgnoreCase() {
		return ignoreCase;
	}

	public String getNullHandling() {
		return nullHandling;
	}

	public boolean isAscending() {
		return ascending;
	}

	public boolean isDescending() {
		return descending;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}

	public void setNullHandling(String nullHandling) {
		this.nullHandling = nullHandling;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	public void setDescending(boolean descending) {
		this.descending = descending;
	}
}
