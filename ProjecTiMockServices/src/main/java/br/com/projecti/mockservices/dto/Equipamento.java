package br.com.projecti.mockservices.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Equipamento {

	private Long codigo_contratacao;
	private Long id_tipo_equipamento;
	private Long id_solucao;
	private String tipo_solucao;
	private String descricao_alias_sistemas;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
	private LocalDateTime data_contratacao;
	private List<Solicitacao> solicitacoes = new ArrayList<>();
	private Detalhe detalhes;
	private Financeiro financeiro;
	private Long codigoContratoSolucao;

	public Long getCodigo_contratacao() {
		return codigo_contratacao;
	}

	public Long getId_tipo_equipamento() {
		return id_tipo_equipamento;
	}

	public Long getId_solucao() {
		return id_solucao;
	}

	public String getTipo_solucao() {
		return tipo_solucao;
	}

	public String getDescricao_alias_sistemas() {
		return descricao_alias_sistemas;
	}

	public LocalDateTime getData_contratacao() {
		return data_contratacao;
	}

	public Detalhe getDetalhes() {
		return detalhes;
	}

	public Financeiro getFinanceiro() {
		return financeiro;
	}

	public void setCodigo_contratacao(Long codigo_contratacao) {
		this.codigo_contratacao = codigo_contratacao;
	}

	public void setId_tipo_equipamento(Long id_tipo_equipamento) {
		this.id_tipo_equipamento = id_tipo_equipamento;
	}

	public void setId_solucao(Long id_solucao) {
		this.id_solucao = id_solucao;
	}

	public void setTipo_solucao(String tipo_solucao) {
		this.tipo_solucao = tipo_solucao;
	}

	public void setDescricao_alias_sistemas(String descricao_alias_sistemas) {
		this.descricao_alias_sistemas = descricao_alias_sistemas;
	}

	public void setData_contratacao(LocalDateTime localDateTime) {
		this.data_contratacao = localDateTime;
	}

	public void setDetalhes(Detalhe detalhes) {
		this.detalhes = detalhes;
	}

	public void setFinanceiro(Financeiro financeiro) {
		this.financeiro = financeiro;
	}

	public List<Solicitacao> getSolicitacoes() {
		return solicitacoes;
	}

	public void setSolicitacoes(List<Solicitacao> solicitacoes) {
		this.solicitacoes = solicitacoes;
	}

	public Long getCodigoContratoSolucao() {
		return codigoContratoSolucao;
	}

	public void setCodigoContratoSolucao(Long codigoContratoSolucao) {
		this.codigoContratoSolucao = codigoContratoSolucao;
	}

}
