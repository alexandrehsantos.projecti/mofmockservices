package br.com.projecti.mockservices.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projecti.mockservices.dao.MockRestDao;
import br.com.projecti.mockservices.dto.FakeBeneficioConcessaoDto;
import br.com.projecti.mockservices.dto.JsonDinamicoDTO;
import br.com.projecti.mockservices.entity.MockRestJson;

@Service
public class BeneficioConcedidoController {

	@Autowired
	MockRestDao mockRestDao;
	
	@Transactional
	public void salvar(String jsonDinamicoDTO) {
		MockRestJson mockRestJson = new MockRestJson();
		
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setJson(jsonDinamicoDTO.toString());
		
		mockRestDao.save(mockRestJson);
	}
}
