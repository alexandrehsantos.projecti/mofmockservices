package br.com.projecti.mockservices.dto;

public class Financeiro {

	private Double valor_padrao;
	private String tarifa_unica;

	public Double getValor_padrao() {
		return valor_padrao;
	}

	public String getTarifa_unica() {
		return tarifa_unica;
	}

	public void setValor_padrao(Double valor_padrao) {
		this.valor_padrao = valor_padrao;
	}

	public void setTarifa_unica(String tarifa_unica) {
		this.tarifa_unica = tarifa_unica;
	}

}
