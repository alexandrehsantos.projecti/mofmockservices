package br.com.projecti.mockservices.resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.WebServiceException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MdrBandeiraModalidade {

	@GetMapping("/trp/corpclienteapi/estabelecimento/{codigoEC}/taxas-mdr/")
	public ResponseEntity listarMdrBandeiraModalidade(@PathVariable String codigoEC) {

		if (codigoEC == null) {

			throw new WebServiceException("Codigo Ec é obrigatório");
		}

		Map<Object, Object> listaMdr1 = new HashMap<>();
		Map<Object, Object> listaMdr2 = new HashMap<>();
		Map<Object, Object> listaMdr3 = new HashMap<>();

		listaMdr1.put("cdBandeira", "A");
		listaMdr1.put("valorMDR", new BigDecimal("333.99"));
		listaMdr1.put("nmModalidade", "Modalidade 1");
	
		listaMdr2.put("cdBandeira", "B");
		listaMdr2.put("valorMDR", new BigDecimal("99.99"));
		listaMdr2.put("nmModalidade", "Modalidade 2");
		
		listaMdr3.put("cdBandeira", "C");
		listaMdr3.put("valorMDR", new BigDecimal("2000"));
		listaMdr3.put("nmModalidade", "Modalidade 3");

		List<Map<Object, Object>> lista = new ArrayList<>();

		lista.add(listaMdr1);
		lista.add(listaMdr2);
		lista.add(listaMdr3);

		return ResponseEntity.ok(lista);

	}

}
