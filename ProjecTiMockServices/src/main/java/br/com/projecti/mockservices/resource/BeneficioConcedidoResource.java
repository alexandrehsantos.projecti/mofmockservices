package br.com.projecti.mockservices.resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.projecti.mockservices.controller.BeneficioConcedidoController;
import br.com.projecti.mockservices.dto.FakeBeneficioConcessaoDto;
import br.com.projecti.mockservices.dto.JsonDinamicoDTO;

@RestController
public class BeneficioConcedidoResource {
	@Autowired
	BeneficioConcedidoController beneficioConcedidoController;

	@PutMapping("/trp/corpnegocioapi/estabelecimento/aluguel-equipamento")
	public ResponseEntity concessao(@RequestBody JsonDinamicoDTO jsonDinamicoDTO) {
		Map<Object, Object> jsonValue = new HashMap<>();
		jsonValue.put("valorCondicaoUnica", new BigDecimal(69.9));

		return ResponseEntity.ok(jsonValue);
	}
	
	@PutMapping("/trp/corpnegocioapi/estabelecimento/beneficio-concedido")
	public ResponseEntity enviaBeneficio(@RequestBody String jsonDinamicoDTO) {
		System.out.println("JSON->"+jsonDinamicoDTO);
		beneficioConcedidoController.salvar(jsonDinamicoDTO);
		return ResponseEntity.ok(jsonDinamicoDTO);
	}

	@GetMapping("/trp/corpnegocioapi/estabelecimento/aluguel-equipamento/lista")
	public ResponseEntity concessaoLista() {

		HashMap<Object, Object> objeto1 = new HashMap<>();
		HashMap<Object, Object> objeto2 = new HashMap<>();
		HashMap<Object, Object> objeto3 = new HashMap<>();

		objeto1.put("identificadorObjetoListaComponente", getRandomId() + " - mock");
		objeto1.put("nomeObjetoListaComponente", "Facke Retorno List (mock)");
		objeto1.put("dataObjetoListaComponente", "2018-08-10 15:31:41.909");
		objeto1.put("valorBaseAplicacaoBeneficioObjetoListaComponente", new BigDecimal(20.0));

		objeto2.put("identificadorObjetoListaComponente", getRandomId() + " - mock");
		objeto2.put("nomeObjetoListaComponente", "Facke Retorno List (mock)");
		objeto2.put("dataObjetoListaComponente", "2018-08-10 15:31:41.909");
		objeto2.put("valorBaseAplicacaoBeneficioObjetoListaComponente", new BigDecimal(25.0));

		objeto3.put("identificadorObjetoListaComponente", getRandomId() + " - mock");
		objeto3.put("nomeObjetoListaComponente", "Facke Retorno List (mock)");
		objeto3.put("dataObjetoListaComponente", "2018-08-10 15:31:41.909");
		objeto3.put("valorBaseAplicacaoBeneficioObjetoListaComponente", new BigDecimal(30.0));

		List<HashMap<Object, Object>> lista = new ArrayList<>();

		lista.add(objeto1);
		lista.add(objeto2);
		lista.add(objeto3);

		return ResponseEntity.ok(lista);
	}

	private int getRandomId() {
		Random r = new Random();
		return r.ints(1, (10000000 + 1)).findFirst().getAsInt();
	}

}
