package br.com.projecti.mockservices.resource;

import java.util.List;

public class ContentRootDto {

	private List<ContentDto> content;

	public List<ContentDto> getContent() {
		return content;
	}

	public void setContent(List<ContentDto> content) {
		this.content = content;
	}

}
