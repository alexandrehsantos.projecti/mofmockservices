package br.com.projecti.mockservices.resource;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.projecti.mockservices.dao.MockRestDao;
import br.com.projecti.mockservices.entity.MockRestJson;

@Controller
public class BackOfficeFormData {

	@Autowired
	private MockRestDao mockdao;
	
	@RequestMapping(value = "/solicitacao", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity createRole(@RequestBody String values) {
		System.out.println(values);
		
		MockRestJson mockRestJson = new MockRestJson();
		mockRestJson.setDataCriacao(new Date());
		mockRestJson.setCallerDescription("Serviço usado para solicitações de fila de trabalho (BackOffice");
		mockRestJson.setJson(values);
		
		mockRestJson.setUrlMock("/solicitacao");
		mockdao.save(mockRestJson);
		
		return ResponseEntity.ok(values);
	}

}
