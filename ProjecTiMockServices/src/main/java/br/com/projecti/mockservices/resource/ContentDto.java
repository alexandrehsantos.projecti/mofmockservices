package br.com.projecti.mockservices.resource;

public class ContentDto {

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
