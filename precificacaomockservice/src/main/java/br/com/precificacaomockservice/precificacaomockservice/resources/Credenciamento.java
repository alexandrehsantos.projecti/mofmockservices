package br.com.precificacaomockservice.precificacaomockservice.resources;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("trp/credenciamentoapi")
public class Credenciamento {
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/buscar-proposta")
	public ResponseEntity getBuscarProposta(@RequestParam Long cnpj, @RequestParam Long codigoTricard, @RequestParam Long numeroDaProposta) throws IOException {
		InputStream input = getClass().getClassLoader().getResourceAsStream("buscar-proposta-mock.json");

		String content = IOUtils.toString(input, "UTF-8");

		return ResponseEntity.ok(content);
	}

}