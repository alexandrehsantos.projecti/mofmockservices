package br.com.precificacaomockservice.precificacaomockservice.resources;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("activeDirectory")
public class ActiveDirectory {
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/usuarios/sistemas/PREC")
	public ResponseEntity getUsuariosAdPrecificacao() throws IOException {
		InputStream input = getClass().getClassLoader().getResourceAsStream("ad-mock.json");

		String content = IOUtils.toString(input, "UTF-8");

		return ResponseEntity.ok(content);
	}
	@SuppressWarnings("rawtypes")
	@GetMapping("/usuarios/sistemas/TRPBO")
	public ResponseEntity getUsuariosAdBackoffice() throws IOException {
		InputStream input = getClass().getClassLoader().getResourceAsStream("ad-mock.json");
		
		String content = IOUtils.toString(input, "UTF-8");
		
		return ResponseEntity.ok(content);
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/usuarios/sistemas/TRPCRED")
	public ResponseEntity getUsuariosAdCredenciamento() throws IOException {
		InputStream input = getClass().getClassLoader().getResourceAsStream("ad-mock.json");
		
		String content = IOUtils.toString(input, "UTF-8");
		
		return ResponseEntity.ok(content);
	}
	@SuppressWarnings("rawtypes")
	@GetMapping("/usuarios/{usuario}")
	public ResponseEntity getUsuarioDetalhe(@PathVariable("usuario") String usuario) throws IOException {
		InputStream input = getClass().getClassLoader().getResourceAsStream("ad-mock.json");
		
		String content = IOUtils.toString(input, "UTF-8");
		
		return ResponseEntity.ok(content);
	}
	@SuppressWarnings("rawtypes")
	@GetMapping("/usuarios/sistemas/{sistemas}")
	public ResponseEntity getUsuarios(@PathVariable("sistemas") String sistemas) throws IOException {
		InputStream input = getClass().getClassLoader().getResourceAsStream("ad-mock.json");
		
		String content = IOUtils.toString(input, "UTF-8");
		
		return ResponseEntity.ok(content);
	}

}